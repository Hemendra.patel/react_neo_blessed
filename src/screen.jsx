import React from "react";
import blessed from "neo-blessed";
import { createBlessedRenderer } from "react-blessed";
import { createMemoryHistory } from "history";
import AppRoute from "./route/app-route";
import { Router, Switch, Route } from "react-router";

export function buildscreen(client) {
  const screen = blessed.screen({
    focused: true,
    grabKeys: true,
    autoPadding: true,
    input: client,
    output: client,
    mouse: true,
    smartCSR: true,
    enableKeys: true,
    enableInput: true,
    useBCE: true,
    fullUnicode: true,
    width: "100%",
    height: "100%",
    title: "react-rfui",
  });
  const history = createMemoryHistory();
  const render = createBlessedRenderer(blessed);
  render(
    <box width="100%" height="55%">
      <Router history={history}>
        <Switch>
          {AppRoute.map(({ component, exact, path, isAutheticated }, i) => (
            <Route path={path} exact={exact} component={component} />
          ))}
        </Switch>
      </Router>
    </box>,
    screen
  );

  return screen;
}
