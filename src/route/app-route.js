import Login from "../components/login/login.component";
import LanguageComponent from "../components/language/language.component";
const AppRoute = [{
    path: "/language",
    exact: true,
    component: LanguageComponent
  },
  {
    path: "/",
    exact: true,
    component: Login
  },
];
export default AppRoute;