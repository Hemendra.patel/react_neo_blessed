import React, { useRef, useEffect } from "react";
import _languages from "./language.json";
import { useHistory } from "react-router-dom";
import intl from "react-intl-universal";
const LanguageComponent = () => {
  let listRef = useRef(null);
  let languages = _languages;
  const history = useHistory();
  const languagesForDisplay = languages.map((item) => item.DisplayText);

  const locales = (ch, key) => {
    console.log(listRef.current.value);
    if (key && key.full === "enter") {
      const currentLocale = languages.find(
        (item) => item.DisplayText === listRef.current.value
      );
      intl.init({
        currentLocale,
        locales: {
          [currentLocale]: require(`../../locales/${currentLocale.Lan}`),
        },
      });
      history.push("/menu");
    }
  };
  useEffect(() => {
    if (!listRef.current.focused) listRef.current.focus();
  }, []);
  const styleList = {
    selected: {
      bg: "blue",
      fg: "white",
    },
    item: {
      bg: "white",
      fg: "blue",
    },
  };
  const styleInput = {
    boarder: 1,
    focus: {
      bg: "red",
    },
  };

  return (
    <form focused keys left="0" top="0" width="100%" height="100%">
      <text
        top={1}
        left={2}
        height={1}
        content="Please select a language to continue"
      ></text>

      <list
        id="menulist"
        top={2}
        left={2}
        height={5}
        scrollable
        border={{
          type: "line",
        }}
        keys
        onKeypress={locales}
        ref={listRef}
        items={languagesForDisplay}
        style={styleList}
      />
    </form>
  );
};

export default LanguageComponent;
