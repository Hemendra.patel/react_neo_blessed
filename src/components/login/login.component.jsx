import React, { useRef, useEffect } from "react";
import { useHistory } from "react-router-dom";
const LoginComponent = () => {
  const history = useHistory();
  let nameRef = useRef(null);
  let passwordRef = useRef(null);

  const submit = () => {
    history.push("/language");
  };
  useEffect(() => {
    if (!nameRef.current.focused) nameRef.current.focus();
  }, []);
  const styleInput = {
    boarder: 1,
    focus: {
      bg: "red",
    },
  };

  return (
    <form keys focused left="0" top="0" width="100%" height="100%">
      <text top={2} left={2} height={1} content="Name"></text>

      <textbox
        focused
        id="txtUserName"
        top={1}
        left={10}
        height={3}
        inputOnFocus
        keys
        onSubmit={submit}
        ref={nameRef}
        style={styleInput}
      />
      <text top={6} left={1} height={3} content="Password"></text>
      <textbox
        id="txtPassword"
        censor
        top={5}
        left={10}
        height={3}
        keys
        inputOnFocus
        ref={passwordRef}
        style={styleInput}
        onSubmit={submit}
      />
    </form>
  );
};

export default LoginComponent;
