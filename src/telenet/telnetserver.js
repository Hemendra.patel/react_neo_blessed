const _telnet = require('telnet2');
const _rfScreen = require('../screen');
const telnetPort = '2001';


const telnetServer = _telnet({
  tty: true,
}, (client) => {
  let rfScreen = {};
  client.columns = 30;
  client.rows = 20;

  client.once('term', (terminal) => {
    try {
      rfScreen = _rfScreen.buildscreen(client);
      // let rfScreenFactory = _rfScreen.screenFactory(client);
      // let clientScreen = rfScreenFactory.createScreen();
      // rfScreen = rfScreenFactory.buildMasterPage(clientScreen);


      let telnetTerminal = terminal;
      let terminalInfo = terminal.split('-');
      rfScreen.title = `react`;

      telnetTerminal = terminalInfo.join('-');
      rfScreen.userAgent = telnetTerminal || 'xterm-256';



    } catch (error) {
      // error.stack = error.stack.concat('\n' + JSON.stringify((rfScreen || {}).sessionId));
      throw error;
    }
  });

});

telnetServer.listen(telnetPort, () => {
  console.log('Telnet server listening on port ' + telnetPort);
});